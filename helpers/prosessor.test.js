const prosessor = require('./prosessor')

test('fooma barbu', () => {
  expect(prosessor('fooma barbu')).toStrictEqual('bama foorbu')
})

test('hello', () => {
  expect(prosessor('hello')).toStrictEqual('hello')
})

test('amama   bomomo foo', () => {
  expect(prosessor('amama   bomomo foo')).toStrictEqual('bomama   amomo foo')
})

test("I'd rather die here.", () => {
  expect(prosessor("I'd rather die here.")).toStrictEqual(
    "ra'd Ither he diere."
  )
})

test('vuoirkage mäölnö', () => {
  expect(prosessor('vuoirkage mäölnö')).toStrictEqual('mäörkage vuoilnö')
})

test('Donald Trump', () => {
  expect(prosessor('Donald Trump')).toStrictEqual('Trunald Domp')
})

test('Supercalifragilisticexpialidocious!', () => {
  expect(prosessor('Supercalifragilisticexpialidocious!')).toStrictEqual(
    'Supercalifragilisticexpialidocious!'
  )
})

test('death, famine, and pestilence', () => {
  expect(prosessor('death, famine, and pestilence')).toStrictEqual(
    'fath, deamine, pend astilence'
  )
})
