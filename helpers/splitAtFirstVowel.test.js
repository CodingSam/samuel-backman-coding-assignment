const splitAtFirstVowel = require('./splitAtFirstVowel')

test('zero vowel', () => {
  expect(splitAtFirstVowel('vbnm')).toStrictEqual(['vbnm', ''])
})

test('single vowel', () => {
  expect(splitAtFirstVowel('fama')).toStrictEqual(['fa', 'ma'])
})

test('double vowel', () => {
  expect(splitAtFirstVowel('fooma')).toStrictEqual(['foo', 'ma'])
})

test('triple vowel', () => {
  expect(splitAtFirstVowel('foooma')).toStrictEqual(['fooo', 'ma'])
})

test('vowel first', () => {
  expect(splitAtFirstVowel('ooomaa')).toStrictEqual(['ooo', 'maa'])
})

test('vowels last', () => {
  expect(splitAtFirstVowel('fkkkmaaa')).toStrictEqual(['fkkkmaaa', ''])
})

test('special chars', () => {
  expect(splitAtFirstVowel("fk,,kkma'aa,")).toStrictEqual(['fk,,kkma', "'aa,"])
})

test('space', () => {
  expect(splitAtFirstVowel(' ')).toStrictEqual([' ', ''])
})

test('space in beginning', () => {
  expect(splitAtFirstVowel(' lala')).toStrictEqual([' la', 'la'])
})

test('space in end', () => {
  expect(splitAtFirstVowel('lala ')).toStrictEqual(['la', 'la '])
})
