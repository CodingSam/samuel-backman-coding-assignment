const vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'å', 'ä', 'ö']
const isCharacterVowel = (char) => vowels.includes(char.toLowerCase())

module.exports = (word) => {
  let splitIndex = 0

  for (let index = 0; index < word.length - 1; index++) {
    if (isCharacterVowel(word[index])) {
      let checkNextChar = true
      let offset = 1

      do {
        if (isCharacterVowel(word[index + offset])) {
          offset++
        } else {
          checkNextChar = false
        }
      } while (checkNextChar && index + offset < word.length)

      splitIndex = index + offset
      break
    }
  }

  if (splitIndex === 0 || splitIndex === word.length) {
    return [word, '']
  }

  return [word.slice(0, splitIndex), word.slice(splitIndex)]
}
