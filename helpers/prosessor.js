const splitAtFirstVowel = require('./splitAtFirstVowel')

module.exports = (string) => {
  let parts = string.split(/(\s+)/) // split with spaces and keep the spaces

  if (parts.length === 1) {
    return parts[0]
  }

  let prosessedParts = parts.map((part) => {
    if (part.trim().length === 0) {
      return part
    }
    return splitAtFirstVowel(part)
  })

  let firstVowelIndex = null
  let readyParts = []

  prosessedParts.forEach((part, index) => {
    if (Array.isArray(part)) {
      if (firstVowelIndex === null) {
        firstVowelIndex = index
        readyParts.push(part)
      } else if (firstVowelIndex !== null) {
        const vowel1 = readyParts[firstVowelIndex][0]
        const vowel2 = part[0]

        readyParts[firstVowelIndex][0] = vowel2
        part[0] = vowel1
        readyParts.push(part)
        firstVowelIndex = null
      }
    } else {
      readyParts.push(part)
    }
  })

  return readyParts
    .map((d) => {
      if (Array.isArray(d)) {
        return `${d.join('')}`
      } else {
        return d
      }
    })
    .join('')
}
