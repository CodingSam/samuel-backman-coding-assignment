const express = require('express')
const app = express()
const port = 3000

const prosessor = require('./helpers/prosessor')

app.use(express.text({ type: '*/*' }))
app.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body = JSON.parse(decodeURIComponent(req.body))
  }
  next()
})

app.get('/', (req, res) => {
  res.send('Sorry nothing here, try doing a post request')
})

app.post('/', (req, res) => {
  const result = prosessor(req.body)
  res.json(result)
})

app.listen(port, () => {
  console.log(`Listening on :${port}`)
})
